import Vue from 'vue'
import Router from 'vue-router'
import index from '../pages/index'
import history from '../pages/history'
Vue.use(Router);


const routes = [
    {
      path: '/',
      component: index,
      name: 'index'
    },
    {
      path: '/history/:id',
      name: 'history',
      component: history,
      props: true
    },

  ];

export function createRouter() {
  return new Router({
    mode: 'history',
    routes
  });
}
