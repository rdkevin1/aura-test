'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PeopleSchema extends Schema {
  up () {
    this.create('people', (table) => {
      table.increments('id');
      table.string('name');
      table.string('age');
      table.string('location');
      table.enu('gender',['M','F']);
      table.timestamps();
    })
  }

  down () {
    this.drop('people')
  }
}

module.exports = PeopleSchema
