'use strict'

/*
|--------------------------------------------------------------------------
| PersonSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class PersonSeeder {
  async run () {
    const persons = await Factory.model('App/Models/Person').createMany(30);

    const gender = ['M', 'F']
    for(let person of persons) {
      let randomNumber = Math.floor(Math.random()*gender.length);
      person.gender = gender[randomNumber];
      let point = await Factory.model('App/Models/Point').create();
      await person.save();
      await person.points().save(point);
    }
  }
}

module.exports = PersonSeeder
