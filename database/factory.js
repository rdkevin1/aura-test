'use strict'

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

 const Factory = use('Factory');

  Factory.blueprint('App/Models/Person', (faker) => {
    return {
      name: faker.name(),
      age: faker.age(),
      location: faker.city(),
    }
  });

Factory.blueprint('App/Models/Point', (faker) => {
  return {
    points: '0',
  }
});
