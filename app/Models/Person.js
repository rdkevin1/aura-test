'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

class Person extends Model {
  points () {
    return this.hasMany('App/Models/Point', 'id','people_id')
  }
}

module.exports = Person;
