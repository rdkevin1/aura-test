'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');

class Point extends Model {
  person () {
    return this.belongsTo('App/Models/Person','id','people_id')
  }
}

module.exports = Point;
