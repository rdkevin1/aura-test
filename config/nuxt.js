const { resolve } = require('path')

require("dotenv").config();

module.exports = {
  mode: 'universal',
  dev: process.env.NODE_ENV === 'development',
  srcDir: resolve(__dirname, '..', 'resources'),
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    { src: "../resources/assets/sass/app.scss", lang: "scss" }
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    "../resources/plugins/bootstrap-vue.js"
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    "@nuxtjs/router",
    ["@nuxtjs/dotenv", { systemvars: true, path: resolve(__dirname, '..') }],
  ],

  routerModule: {
    path: resolve(__dirname, '..', 'resources/routes'),
    fileName: "router.js"
  },
  /*
  ** Nuxt.js modules
  */
  modules: [
  ],
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  }
}
