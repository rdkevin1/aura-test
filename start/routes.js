'use strict'
const Database = use('Database')
const Persons = use('App/Models/Person');
/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/guides/routing
|
*/

const Route = use('Route');

Route.get('/people', async () =>{
  let peoples = await Persons.all();
  return peoples;
});

Route.get('/people/:id/info', async({params}) =>{
  let people = Persons.find(params.id);
  return people;
});

Route.get('/people/:id/points', async ({ params }) =>{
 let points = await Database.table('points').where('people_id', params.id);
 return points;
});

Route.post('/people/:id/create/point', async({request, params}) => {
  let people = await Persons.find(params.id);
  let point = people.points().create(request.all());
  return point;
});

Route.any('*', 'NuxtController.render');
